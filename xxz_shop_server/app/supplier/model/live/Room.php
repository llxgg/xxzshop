<?php

namespace app\supplier\model\live;

use app\common\model\plus\live\Room as RoomModel;

/**
 * 房间模型
 */
class Room extends RoomModel
{
    /**
     * 列表
     */
    public function getList($params,$shop_supplier_id)
    {
        return $this->with(['share', 'user','cover'])
            ->where('shop_supplier_id','=',$shop_supplier_id)
            ->order(['create_time' => 'desc'])
            ->paginate($params, false, [
                'query' => request()->request()
            ]);
    }

    /**
     * 删除记录 (软删除)
     */
    public function setDelete($where)
    {
        return self::update(['is_delete' => 1], $where);
    }

    public function edit($data)
    {
        return $this->save([
            'virtual_num' => $data['virtual_num'],
            'is_top' => $data['is_top'],
            'is_delete' => $data['is_delete'],
            'sort' => $data['sort'],
        ]);
    }
    

}
