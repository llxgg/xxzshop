<?php

namespace app\shop\controller;

use app\shop\model\shop\Sms as SmsModel;
use app\shop\model\shop\User;
use think\facade\Session;
use think\exception\ValidateException;
use app\shop\validate\shop\User as ValidateUser;

/**
 * 商户认证
 */
class Passport extends Controller
{
    /**
     * 商户后台登录
     */
    public function login()
    {
        //登录前清空session
        session('xxzshop_store', null);
        $user = $this->postData();
        $user['password'] = salt_hash($user['password']);
        $model = new User();
        if ($model->checkLogin($user)) {
            return $this->renderSuccess('登录成功', $user['username']);
        }
        return $this->renderError($model->getError()?:'登录失败');
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        session('xxzshop_store', null);
        return $this->renderSuccess('退出成功');
    }

    /*
   * 修改密码
   */
    public function editPass()
    {
        $model = new User();
        if ($model->editPass($this->postData(), $this->store['user'])) {
            return $this->renderSuccess('修改成功');
        }
        return $this->renderError($model->getError()?:'修改失败');
    }

    /**
     * 获取验证码
     */
    public function sendCode()
    {

        session('xxzshop_store', null);
        $data = $this->postData();
        $result = $this->validateData($data,'mobile');
        
        if ( !$result['check'] ){
           return $this->renderError($result['msg']);
        }
    
        $model = new SmsModel();
        if ($model->send($data['mobile'], $data['type'])) {
            return $this->renderSuccess();
        }
        return $this->renderError($model->getError() ?: '发送失败');
    }

    /**
     * 短信注册
     */
    public function smsRegister()
	{
        session('xxzshop_store', null);
        $data = $this->postData();
        $result = $this->validateData($data,'code');
        
        if ( !$result['check'] ){
           return $this->renderError($result['msg']);
        }

        $model = new User();
 
        if ($model->smsregister($data)) {
            return $this->renderSuccess();
        }
        return $this->renderError($model->getError() ?: '登录失败');
    }

    /**
     * 短信登录
     */
    public function smsLogin()
	{
        //登录前清空session
        session('xxzshop_store', null);
        $user = $this->postData();
        $result = $this->validateData($user,'code');
        
        if ( !$result['check'] ){
           return $this->renderError($result['msg']);
        }
 
        $model = new User();
        
        if ($model->smslogin($user)) {
            return $this->renderSuccess('登录成功', $user['mobile']);
        }
        
        return $this->renderError($model->getError() ?: '登录失败');
    }

    /**
     * 重设密码
     */
    public function resetPassword()
    {

        session('xxzshop_store', null);
        $data = $this->postData();
        $result = $this->validateData($data,'code');
        
        if ( !$result['check'] ){
           return $this->renderError($result['msg']);
        }
        
    

     
        $model = new User();
        if ($model->resetpassword($data)) {
            return $this->renderSuccess('设置成功');
        }
        return $this->renderError($model->getError() ?: '设置失败');

    }
    

    private function validateData($data, $scene = 'mobile')
    {
        $result = [];
        $result['check'] = false;
        
        try {
            validate(ValidateUser::class)
                ->scene($scene)
                ->check($data);
        } catch (ValidateException $e) {
            // 验证失败 输出错误信息
               $result['msg'] = ($e->getError());
                return  $result;
        }    
        
        $result['check'] = true;
        return $result;
    }
}
