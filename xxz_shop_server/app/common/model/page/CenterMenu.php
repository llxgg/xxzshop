<?php

namespace app\common\model\page;

use think\facade\Request;
use app\common\model\BaseModel;

/**
 * 个人中心菜单模型
 */
class CenterMenu extends BaseModel
{
    protected $name = 'center_menu';
    protected $pk = 'menu_id';

    /**
     * 获取列表
     */
    public function getList($limit = 20)
    {   
        return $this->withoutGlobalScope()->order(['sort' => 'asc'])
            ->paginate($limit, false, [
                'query' => Request::instance()->request()
            ]);
    }

    /**
     * 详情
     */
    public static function detail($menu_id)
    {
        return self::withoutGlobalScope()->find($menu_id);
    }
  
}