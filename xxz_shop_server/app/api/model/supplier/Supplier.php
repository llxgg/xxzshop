<?php

namespace app\api\model\supplier;

use app\common\model\supplier\Supplier as SupplierModel;
use app\common\model\supplier\User as SupplierUserModel;
use app\api\model\order\Order as OrderModel;
use app\api\model\user\Favorite as FavoriteModel;
use app\common\model\product\Product as ProductModel;
use app\api\model\settings\Setting as SettingModel;
/**
 * 供应商模型类
 */
class Supplier extends SupplierModel
{
   /**
     * 添加
     */
    public function addData($data)
    {
        // 开启事务
        $this->startTrans();
        try {
            if (SupplierUserModel::checkExist($data['user_name'])) {
                $this->error = '用户名已存在';
                return false;
            }
            // 添加供应商
            $this->save($data);
            //添加供应商账号
            $SupplierUserModel = new SupplierUserModel;
            $data['shop_supplier_id'] = $this['shop_supplier_id'];
            $data['is_super'] = 1;
            $SupplierUserModel->save($data);
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            return false;
        }
    }
    public function getUserName($user_name)
    {
        return $this->where('user_name', '=', $user_name)->count();
    }
    //获取店铺信息
    public function getDetail($data,$user){
        $detail = $this->alias('s')->where(['shop_supplier_id'=>$data['shop_supplier_id']])
           ->field("name as store_name,shop_supplier_id,logo_id,(select coalesce(sum(sales_initial+sales_actual),0) from xxzshop_product p where p.shop_supplier_id=s.shop_supplier_id) as product_sales,category_id,server_score,fav_count,user_id")
           ->with(['logo','category'])
           ->find();
        if($detail){
            $detail['logos'] = $detail['logo']['file_path'];
            $detail['category_name'] = $detail['category']['name'];
            unset($detail['logo']);unset($detail['category']);
            $detail['isfollow'] = 0 ;
            if($user){
                $detail['isfollow'] = (new FavoriteModel)
                    ->where('pid', '=', $data['shop_supplier_id'])
                    ->where('user_id', '=', $user['user_id'])
                    ->where('type', '=', 10)
                    ->count();
            }

        }               
        $detail['product_sales'] = $detail['product_sales']?$detail['product_sales']:0;           
        return $detail;               
    }
     //获取微店账号信息
    public function getAccount($shop_supplier_id,$field="*"){
        $detail = $this->where(['shop_supplier_id'=>$shop_supplier_id])->field("$field")->find();
        return $detail;
    }
    //店铺列表
    public function supplierList($param){
        // 排序规则
        $sort = [];
        if ($param['sortType'] === 'all') {
            $sort = ['s.create_time' => 'desc'];
        } else if ($param['sortType'] === 'sales') {
            $sort = ['product_sales' => 'desc'];
        } else if ($param['sortType'] === 'score') {
            $sort = ['server_score' => 'desc'];
        }

        $model = $this;
        if(isset($param['name'])&&$param['name']){
            $model = $model->where('name','like','%'.$param['name'].'%');
        }
        // 查询列表数据
        $list =  $model->alias('s')->with(['logo','category'])
            ->where('s.is_delete', '=', '0')
            ->where('s.is_recycle', '=', 0)
            ->where('s.is_full', '=', 1)
            ->field("s.shop_supplier_id,s.name,s.fav_count,(select sum(sales_initial+sales_actual) as product_sales from xxzshop_product p where p.shop_supplier_id=s.shop_supplier_id) as product_sales,logo_id,category_id,server_score")
            ->order($sort)
            ->paginate($param, false, [
                'query' => \request()->request()
            ]);
        foreach ($list as $key => &$v) {
           $productList = (new ProductModel)
                               ->with(['image.file'])
                               ->where([
                                   'shop_supplier_id'=>$v['shop_supplier_id'],
                                   'product_status'=>10,
                                   'audit_status' => 10,
                                   'is_delete'=>0
                               ])
                               ->order('product_sort asc,product_id desc')
                               ->limit(3)
                               ->field('product_id,product_price,product_name,sales_initial,sales_actual,line_price')
                               ->select();
            $v['productList'] = $productList;  
            $v['product_sales'] = $v['product_sales']?$v['product_sales']:0;
            $v['logos'] = $v['logo']['file_path'];
            $v['category_name'] = $v['category']['name'];
            unset($v['logo']);unset($v['category']);               
        }
        return $list;
    }
}
