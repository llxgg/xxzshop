<?php

namespace app\api\controller\plus\live;

use app\api\controller\Controller;
use app\api\model\plus\live\Plan as PlanModel;
use app\api\model\plus\live\PlanOrder as PlanOrderModel;
/**
 * 充值套餐控制器
 */
class Plan extends Controller
{
    /**
     * 套餐列表
     */
    public function lists()
    {
        $model = new PlanModel();
        $list = $model->getList($this->postData());
        return $this->renderSuccess('', compact('list'));
    }

    /**
     * 充值套餐
     */
    public function submit($plan_id)
    {
        $params = $this->request->param();
        // 用户信息
        $user = $this->getUser();
        // 生成等级订单
        $model = new PlanOrderModel;
        $order_id = $model->createOrder($user, $plan_id, $params['pay_type']);
        if(!$order_id){
            return $this->renderError($model->getError() ?: '购买失败');
        }
        // 在线支付
        $payment = PlanOrderModel::onOrderPayment($user, $model, $params['pay_type'], $params['pay_source']);
        // 返回结算信息
        return $this->renderSuccess(['success' => '支付成功', 'error' => '订单未支付'], [
            'order_id' => $order_id,   // 订单id
            'pay_type' => $params['pay_type'],  // 支付方式
            'payment' => $payment,               // 微信支付参数
        ]);
    }
}