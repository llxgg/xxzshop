 **项目介绍** 

小玄猪商城小程序、多端应用商城，易于二次开发
小玄猪商城基于php、thinkphp6、vue、element-ui、uniapp开发的商城小程序，内置多商户商城和分销商城。包含小程序商城、H5商城、公众号商城、PC商城、App，多种分销模式、拼团、砍价、秒杀、优惠券、活动报名、客户管理、推广素材、品牌、分类商品参数、知识付费、抽奖、积分、会员等级、小程序直播、页面DIY，前后端分离全部100%开源。


 **软件架构** 

后端：php+thinkphp6

前端：vue+element-ui

小程序端：uniapp

部署环境建议：Linux + Nginx + PHP7.1 + MySQL5.6，或者直接用宝塔集成环境。


 **技术特点** 

前后分离(分工协助 开发效率高)

统一权限(前后端一致的权限管理)

uniapp(集成多个主流平台)

thinkphp6(上手简单，极易开发)

element-ui(饿了么前端开源管理后台框架，方便快速开发)


 **系统截图** 

![输入图片说明](1%E5%95%86%E5%9F%8E%E8%A1%8C%E4%B8%9A.jpg)
![输入图片说明](2%E5%95%86%E5%9F%8E%E9%A6%96%E9%A1%B5'.jpg)
![输入图片说明](3%E9%A1%B5%E9%9D%A2%E7%BC%96%E8%BE%91%E5%99%A8'.jpg)
![输入图片说明](4%E5%88%86%E9%94%80-%E5%A5%96%E5%8A%B1%E6%A8%A1%E5%BC%8F'.jpg)
![输入图片说明](%E7%A7%BB%E5%8A%A8%E7%AB%AF%E6%8B%BC%E5%9B%BE1.jpg)
![输入图片说明](%E7%A7%BB%E5%8A%A8%E7%AB%AF%E6%8B%BC%E5%9B%BE2.jpg)


 **特别鸣谢** 

thinkphp:https://www.thinkphp.cn

element-ui:https://element.eleme.cn

vue:https://cn.vuejs.org/

easywechat:https://www.easywechat.com/

